<?php
	include 'inc/clientGPlay.php';
?>
<!DOCTYPE html>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<meta name="viewport" content="width=device-width" />
  	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  	
  	<!-- CHANGE THIS TITLE TAG -->
    <title>Android Developer Web</title>
    
    <!-- media-queries.js -->
    <!--[if lt IE 9]>
    	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <!-- html5.js -->
    <!--[if lt IE 9]>
    	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <link href="./font/stylesheet.css" rel="stylesheet" type="text/css" />	
    <link href="./css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
    <link href="./css/styles.css" rel="stylesheet" type="text/css" />
    <link href="./css/media-queries.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="./fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    

    <link href='http://fonts.googleapis.com/css?family=Exo:400,800' rel='stylesheet' type='text/css'>


  </head>
  <body data-spy="scroll">
	  <!-- TOP MENU NAVIGATION -->
	  <div class="navbar navbar-fixed-top">
	  	<div class="navbar-inner">
	  		<div class="container">
	  	
	  			<a class="brand pull-left" href="#">
	  			
	  			</a>
	  	
	  			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
	  				<span class="icon-bar"></span>
	  				<span class="icon-bar"></span>
	  				<span class="icon-bar"></span>
	  			</a>
	  		
	  			<div class="nav-collapse collapse">
	  				<ul id="nav-list" class="nav pull-right">
	  					<li><a href="#home">Home</a></li>
	  					
	  					<?php
	  						  $apps = loadApps("Google%20Inc.");
	  						  	foreach($apps as $app) {
                                                ?>
	  					
	  					<li><a href="#<?php echo $app->getName(); ?>"><?php echo $app->getName(); ?></a></li>
	  						<?php } ?>
	  					<li><a href="#contact">Contact</a></li>
	  				</ul>
	  			</div>
	  		
	  		</div>
	  	</div>
	  </div>
	  
	  
  	<!-- MAIN CONTENT -->
  	<div class="container content container-fluid" id="home">
  			<?php
  				  
  				  	foreach($apps as $app) {
  				  	    $app = loadApp($app); 
                                            $imagePhone = $app->getPhoneScreenshot();
                                            if (!$imagePhone) {
                                                $imagePhone = 'img/android-ics-screenshot-360x600.png';
                                            }
                                            ?>
  				  	    
  				  
  		<!-- HOME -->
  			<div class="row-fluid" id="<?php echo $app->getName(); ?>">
  		
  				<!-- PHONES IMAGE FOR DESKTOP MEDIA QUERY -->
  				<div class="span5 visible-desktop telef">
                                    <img src="<?php echo $imagePhone; ?>">
  				</div>
  			
  				<!-- APP DETAILS -->
  				<div class="span7">
  			
  					<!-- ICON -->
  					<div class="visible-desktop span2" id="icon">
  						<img src="<?php echo $app->getIcon(); ?>" />
  					</div>
  					
  					<!-- APP NAME -->
  					<div id="app-name" class="span6">
  						<h1><?php echo $app->getName(); ?></h1>
  					</div>
  					
  					<!-- VERSION -->
  					<div id="version">
                                            <span class="version-top label label-inverse">Version: <?php echo $app->getVersion(); ?></span>
  					</div>
  		          
  					<!-- TAGLINE -->
  					<div id="tagline">
  						<!--A Free Responsive Website Template for Your Mobile App-->
  					</div>
  				
  					<!-- PHONES IMAGE FOR TABLET MEDIA QUERY -->
  					<div class="hidden-desktop telef" id="aphones">
                                            <img src="<?php echo $imagePhone; ?>"> 						
  					</div>
  		          
  					<!-- DESCRIPTION -->
  					<div id="description">
  						<?php echo 'Description: '.$app->getDescription(); ?>
  					</div>
  		          
  					<!-- FEATURES --
  					<ul id="features">
  						<li>Fully Responsive HTML/CSS3 Template</li>
  						<li>Built on Bootstrap by Twitter</li>
  						<li>Images and Photoshop Files Included</li>
  						<li>Completely Free!</li>
  					</ul>
  					-->
  					<!-- DOWNLOAD & REQUIREMENT BOX --
  					<div class="download-box">
  						<a href="#"><img src="img/available-on-the-app-store.png"></a>
  					</div>-->
  					<div class="download-box">
                                            <a href="https://play.google.com/store/apps/details?id=<?php echo $app->getPackage(); ?>">
                                                <img src="img/android_app_on_play_logo_large.png">
                                            </a>
  					</div>
  					<div class="download-box"><!--
  						<strong>Requirements:</strong><br>
  						Compatible with iPhone and iPod touch. Requires iPhone OS 2.2 or later. WiFi, Edge, or 3G network connection sometimes required.-->
  					</div>
  					<div class="download-box">
  						<strong>Requirements:</strong><br>
  						Requires Android 2.3 and higher. WiFi, Edge, or 3G network connection sometimes required.
  					</div>
  					
  				</div>
  			</div>	
  			
  			  			
  			<!-- SCREENSHOTS -->
  			<div class="row-fluid" id="screenshots">
  				
  				<h2 class="page-title" id="scroll_up">
  						Screenshots
  						<a href="#home" class="arrow-top">
  						<img src="img/arrow-top.png">
  						</a>
  					</h2>
  					
  				<!-- SCREENSHOT IMAGES ROW 1-->
  					<ul class="thumbnails">
  						<?php foreach($app->getScreenshots() as $img) { ?>
	  						<li class="span3">
	  							<a href="#" rel="gallery" class="thumbnail">
	  								<img src="<?php echo $img; ?>" alt="">
	  							</a>
	  						</li>	
	  					<?php } ?>	<!--end foreach img -->
  					</ul>		
  			</div>
  			
  			
  		<?php } ?> <!--end foreach app -->
		  		  	
			 
		 
		<!-- CONTACT -->
		<div class="row-fluid" id="contact">
		
			<h2 class="page-title" id="scroll_up">
					Contact
					<a href="#home" class="arrow-top">
					<img src="img/arrow-top.png">
					</a>
				</h2>
			
			<!-- CONTACT INFO -->
			<div class="span4" id="contact-info">
				<h3>Contact Us</h3>
				<p>Hello, have you seen my portfolio of Android applications. If you wish to contact me you can use the form below. I promise to answer as soon as possible. Thanks</p>
				<p><a href="mailto:name@email.com">contact@email.com</a></p>
			</div>
			
			<!-- CONTACT FORM -->
			<div class="span7" id="contact-form">
				<form class="form-horizontal">
					<fieldset>
						<div class="control-group">
							<label class="control-label" for="name">Name</label>
							<div class="controls">
								<input class="input-xlarge" type="text" id="name" placeholder="Name">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="email">Email</label>
							<div class="controls">
								<input class="input-xlarge" type="text" id="email" placeholder="name@email.com">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="subject">Subject</label>
							<div class="controls">
								<input class="input-xlarge" type="text" id="subject" placeholder="General Inquiry">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="message">Message</label>
							<div class="controls">
								<textarea class="input-xlarge" rows="3" id="message" placeholder="Your message..."></textarea>
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">SEND</button>
						</div>
					</fieldset>
				</form>
			</div>
			
		</div><!-- end content contact -->
		
	</div>	<!-- end main content -->			 
	 
    
    <!-- FOOTER -->
    <div class="footer container container-fluid">
    
    	<!-- COPYRIGHT - EDIT HOWEVER YOU WANT! -->
    	<div id="copyright">
    		Copyright &copy; 2013 <br>
    		Licensed under <a rel="license" href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>. Built on <a href="http://twitter.github.com/bootstrap/">Bootstrap</a> and <a href="http://www.trippoinc.com/flexapp">flexapp.</a>
    	</div>
    	
    	<!-- CREDIT - PLEASE LEAVE THIS LINK! -->
    	<div id="credits">
    		Credits<a href="#"></a> and <a href="#"></a>.
    	</div>
    
    </div>
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/bootstrap-collapse.js"></script>
    <script src="./js/bootstrap-scrollspy.js"></script>
    <script src="./fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script src="./fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <script src="./js/init.js"></script>
  </body>
</html>



