<?php
// List of packages ids to ignore on read
$ignored_apps = array(
    'com.google.android.inputmethod.korean',
    'com.google.android.inputmethod.pinyin',
    'com.google.android.carhome',
    'com.google.android.maps.mytracks',
    'com.google.android.marvin.talkback',
    'com.google.android.ears',
    'com.google.android.street',
    'com.google.android.voicesearch',
    'com.google.android.apps.finance',
    'com.google.android.apps.gesturesearch',
    'com.google.android.apps.androidify',
    'com.google.android.apps.blogger',
    'com.google.android.voicesearch.x',
    'com.google.android.ytremote',
    'com.google.android.apps.chrometophone',
    'com.google.android.apps.enterprise.dmagent',
    'com.google.tv.mediabrowser',
    'com.google.android.apps.offers',
    'com.google.android.apps.inputmethod.hindi',
    'com.google.android.apps.iosched',
    'com.google.android.apps.panoramiogtv',
    'com.google.android.apps.enterprise.lookup',
    'com.google.android.marvin.intersectionexplorer',
    'com.google.android.apps.enterprise.cpanel',
    'com.google.android.inputmethod.japanese',
    'com.google.android.apps.adk2',
    'com.google.wolff.androidhunt',
    'com.google.android.youtube.googletv'
);

// need chmod 0777 or writable mode.
// $cache_dir = '';

// Cache store in seconds
$cache_store = 60 * 60;
?>
