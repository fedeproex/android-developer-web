<?php

/**
 * Description of Application
 *
 * @author fedeproex
 */
class Application {
    private $name;
    private $package;
    private $rating;
    private $datePublished;
    private $version;
    private $requisites;
    private $category;
    private $downloads;
    private $fileSize;
    private $price;
    private $contentRating;
    private $icon;
    private $description;
    private $whatsNew;
    private $phoneScreenshot;
    private $screenshots;

    function setName($name) {
        $this->name = $name;
    }
    
    function getName() {
        return $this->name;
    }
    
    function setPackage($package) {
        $this->package = $package;
    }
    
    function getPackage() {
        return $this->package;
    }
    
    function setRating($rating) {
        $this->rating = $rating;
    }
    
    function getRating() {
        return $this->rating;
    }
    
    function setVersion($version) {
        $this->version = $version;
    }
    
    function getVersion() {
        return $this->version;
    }
    
    function setDatePublished($datePublished) {
        $this->datePublished = $datePublished;
    }
    
    function getDatePublished() {
        return $this->datePublished;
    }
    
    function setRequisites($requisites) {
        $this->requisites = $requisites;        
    }
    
    function getRequisites() {
        return $this->requisites;
    }
    
    function setCategory($category) {
        $this->category = $category;        
    }
    
    function getCategory() {
        return $this->category;
    }
    
    function setDownloads($downloads) {
        $this->downloads = $downloads;        
    }
    
    function getDownloads() {
        return $this->downloads;
    }
    
    function setFileSize($fileSize) {
        $this->fileSize = $fileSize;        
    }
    
    function getFileSize() {
        return $this->fileSize;
    }
    
    function setPrice($price) {
        $this->price = $price;        
    }
    
    function getPrice() {
        return $this->price;
    }
    
    function setContentRating($contentRating) {
        $this->contentRating = $contentRating;        
    }
    
    function getContentRating() {
        return $this->contentRating;
    }
    
    function setIcon($icon) {
        $this->icon = $icon;        
    }
    
    function getIcon() {
        return $this->icon;
    }
    
    function setDescription($description) {
        $this->description = $description;
    }
    
    function getDescription() {
        return $this->description;
    }
    
    function setWhatsNew($whatsNew) {
        $this->whatsNew = $whatsNew;
    }
    
    function getWhatsNew() {
        return $this->whatsNew;
    }
    
    function setPhoneScreenshot($phoneScreenshot) {
        $this->phoneScreenshot = $phoneScreenshot;
    }
    
    function getPhoneScreenshot() {
        return $this->phoneScreenshot;
    }
    
    function setScreenshots($screenshots) {
        $this->screenshots = $screenshots;
    }
    
    function getScreenshots() {
        return $this->screenshots;
    }
}

?>
