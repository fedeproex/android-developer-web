<?php
include (dirname(__FILE__).'/../model/Application.php');
include (dirname(__FILE__).'/../cache/php_fast_cache.php');
include (dirname(__FILE__).'/../conf/client.conf.php');

if (isset($cache_dir)) {
    phpFastCache::$path = $cache_dir;
}

function loadApps($developerId) {
    $result = phpFastCache::get("cache".$developerId);
    $ignored_apps = $GLOBALS['ignored_apps'];
    if ($result == null) {
        $isSearching = true;
        $result = array();
        $start = 0;
        while ($isSearching) {
            $gplayUrl = 'https://play.google.com/store/apps/developer?id='.$developerId.'&start='.($start*12);
            $content = loadWeb($gplayUrl);
            $dom = new DOMDocument();
            @$dom->loadHTML($content);
            $xpath = new DOMXPath($dom);
            $nodes = $xpath->query('//div[@id="error-section"]');
            if ($start > 10 || $nodes->length > 0) {
                // If start > 10 something is wrong
                $isSearching = false;
                break;
            }

            $nodes = $xpath->query('//a[@class="title"]');
            $index = count($result);
            foreach($nodes as $node) {
                $package = getPackage($node->getAttribute('href'));
                if (!in_array($package, $ignored_apps)) {
                    $app = new Application();
                    $app->setName($node->getAttribute('title'));
                    $app->setPackage(getPackage($node->getAttribute('href')));
                    $result[$index++] = $app;
                }
            }
            $start++;
        }
        phpFastCache::set("cache".$developerId, $result, $GLOBALS['cache_store']);
    }
    return $result;
}

function loadApp($application) {
    $gplayurl = 'https://play.google.com/store/apps/details?id='.$application->getPackage();
    
    $appCache = phpFastCache::get("cache".$gplayurl);
    
    if ($appCache == null) {
        $content = loadWeb($gplayurl);
        $dom = new DOMDocument();
        @$dom->loadHTML($content);
        $xpath = new DOMXPath($dom);

        // Icon
        $nodes = $xpath->query('//div[@class="doc-banner-icon"]/img');
        if ($nodes->length == 1) {
            $img = $nodes->item(0)->getAttribute('src');
            $pos = strrpos($img, '=');
            if ($pos !== FALSE && $pos < strlen($img)) {
                $img = substr($img, 0, $pos);
            }
            $application->setIcon($img);
        }

        // Description
        $nodes = $xpath->query('//div[@itemprop="description"]');
        if ($nodes->length == 1) {
            //$application->setDescription($nodes->item(0)->textContent);
            $application->setDescription(utf8_decode($dom->saveXML($nodes->item(0))));
        }

        // Whats New
        $nodes = $xpath->query('//div[@class="doc-whatsnew-container"]');
        if ($nodes->length == 1) {
            //$application->setDescription($nodes->item(0)->textContent);
            $application->setWhatsNew(utf8_decode($dom->saveXML($nodes->item(0))));
        }

        // Screenshots
        $nodes = $xpath->query('//div[@class="screenshot-carousel-content-container"]/div');
        $images = array();
        $imagesLand = array();
        $index = 0;
        $indexLand = 0;
        $screenshot;
        foreach($nodes as $node) {
            $screenshot = $node->getAttribute('data-baseurl');
            list($width, $height) = getimagesize($screenshot);
            if ($width < $height) {
                $images[$index++] = $screenshot;
            } else {
                $imagesLand[$indexLand++] = $screenshot;
            }
        }
        $application->setScreenshots(array_merge($images, $imagesLand));
        if (count($images) > 0) {
            $application->setPhoneScreenshot($images[0]);
        }

        $metadataNode = $xpath->query('//dl[@class="doc-metadata-list"]')->item(0);
        // set current ad object as new DOMDocument object so we can parse it
        $metadataDoc = new DOMDocument();
        $cloned = $metadataNode->cloneNode(TRUE);
        $metadataDoc->appendChild($metadataDoc->importNode($cloned, True));
        $xpath = new DOMXPath($metadataDoc);

        // Rating
        $nodes = $xpath->query('//div[@itemprop="ratingValue"]');
        if ($nodes->length == 1) {
            $application->setRating($nodes->item(0)->getAttribute('content'));
        }

        // Date Published
        $nodes = $xpath->query('//time[@itemprop="datePublished"]');
        if ($nodes->length == 1) {
            $application->setDatePublished($nodes->item(0)->textContent);
        }

        // Version
        $nodes = $xpath->query('//dd[@itemprop="softwareVersion"]');
        if ($nodes->length == 1) {
            $application->setVersion(utf8_decode($nodes->item(0)->textContent));
        }

        // Requisites
        $nodes = $xpath->query('//dt[@itemprop="operatingSystems"]/following-sibling::dd');
        if ($nodes->length > 1) {
            $application->setRequisites($nodes->item(0)->textContent);
        }

        // Category
        $nodes = $xpath->query('//dd/a[starts-with(@href, "/store/apps/category")]');
        if ($nodes->length == 1) {
            $application->setCategory($nodes->item(0)->textContent);
        }

        // Downloads
        $nodes = $xpath->query('//dd[@itemprop="numDownloads"]');
        if ($nodes->length == 1) {
            $node = $nodes->item(0);
            $div = $node->getElementsByTagName('div')->item(0);
            $node->removeChild($div);
            $application->setDownloads($node->textContent);
        }

        // File Size
        $nodes = $xpath->query('//dd[@itemprop="fileSize"]');
        if ($nodes->length == 1) {
            $application->setFileSize($nodes->item(0)->textContent);
        }

        // Price
        $nodes = $xpath->query('//span[@itemprop="price"]');
        if ($nodes->length == 1) {
            $application->setPrice($nodes->item(0)->getAttribute('content'));
        }

        // Content Rating
        $nodes = $xpath->query('//dd[@itemprop="contentRating"]');
        if ($nodes->length == 1) {
            $application->setContentRating($nodes->item(0)->textContent);
        }
        phpFastCache::set("cache".$gplayurl, $application, $GLOBALS['cache_store']); // One hour
    } else {
        $application = $appCache;
    }
    return $application;
}

function loadWeb($url) {
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    $output = curl_exec($curl);
    curl_close($curl);
    return $output;
}

function getPackage($url) {
    $pos = strrpos($url, '=');
    if ($pos !== FALSE && $pos < strlen($url)) {
        return substr($url, $pos + 1);
    }
    return $url;
}
?>
